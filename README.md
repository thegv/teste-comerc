# Comerc Test
Esse projeto foi criado com o [Create React App](https://github.com/facebookincubator/create-react-app).

Estamos utilizando o seguinte Stack de tecnologias:

* React (https://reactjs.org/docs)
* SCSS (SASS) (https://sass-lang.com/)
* Webpack (https://webpack.js.org/)
* Node (https://nodejs.org/en/)
* Yarn (https://yarnpkg.com/en/)

Para que o projeto funcione, você primeiro deve se certificar que possui o Node e o Yarn (ou NPM) instalados e funcionando corretamente. Após isso siga as seguintes instruções para rodar o projeto:

* Baixe ou clone este repositório em sua máquina local.
* Acesse a pasta onde estão os arquivos do projeto via terminal
* rode o comando `yarn install` ou `npm install` e aguarde o término do processo
* rode o comando `yarn start` ou `npm start` e pronto! O projeto irá iniciar no endereço localhost:3000 caso não esteja nada rodando nesta porta, senão ele irá procurar por uma nova.

## Observações
Para fins de desenvolvimento do teste apenas, os dados estão 'mockados' em um arquivo chamado `fakedata.json` que contém os modelos pré-concebidos dos items do grid em si.

Este projeto não possui uma suíte completa de testes, portanto, na vida real, escreva testes sempre :)

