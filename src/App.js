import React, { Component } from 'react';
import GridItem from './components/GridItem';
import Header from './components/Header';

import MasonryLayout from 'react-masonry-layout'
import * as fakedata from './fakedata.json' 


class App extends Component {

  constructor() {
    super()
    this.state = {
      perPage: 20,
      items: [] // just for development purposes
    }

    this.loadItems = this.loadItems.bind(this)
  }
  

  loadItems() {
    let newItems = []
    
    for(let i = this.state.perPage; i > 0; i--) {
      let prop = fakedata[Math.floor( Math.random() * fakedata.length)] // get a random array member.

      newItems.push(<GridItem key={i} {...prop} />)  
    }  
    
    this.setState({
      items: this.state.items.concat(newItems)
    })
    
  }

  componentDidMount() {      
      this.loadItems()      
  }

  render() {     
    return (
      <div className="App">

        <Header />

        <MasonryLayout
          className="wrapper masonry"
          infiniteScroll={this.loadItems}
          sizes={[
             { columns: 1, gutter: 20 }, 
             { mq: '768px', columns: 3, gutter: 20 }, 
             { mq: '1024px', columns: 4, gutter: 30 } 
            ]}
          id="masonry-layout">
          
          {this.state.items.map((v, i) => this.state.items[i] )}
        
        </MasonryLayout>
      </div>
    );
  }
}

export default App;
