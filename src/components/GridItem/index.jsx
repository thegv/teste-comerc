import React, { Component } from 'react'
import './styles.css'

class GridItem extends Component {
    render() {
        let props = this.props
        return (
            <article className="grid__item">
                <div className="grid__image">
                    <img src={props.image} alt=""/>
                </div>
                
                <header className="grid__header">
                    <h3 className="grid__title">{props.title}</h3>
                    <h5 className="grid__category">
                        <svg 
                            version="1.1" 
                            xmlns="http://www.w3.org/2000/svg" 
                            width="32" 
                            height="32" 
                            viewBox="0 0 32 32">
                            <path d="M15.938 32c0 0-9.938-14.062-9.938-20.062 0-11.813 9.938-11.938 9.938-11.938s10.062 0.125 10.062 11.875c0 6.187-10.062 20.125-10.062 20.125zM16 6c-2.209 0-4 1.791-4 4s1.791 4 4 4 4-1.791 4-4-1.791-4-4-4z"></path>
                        </svg>
                        {props.category}
                    </h5>
                </header>

                <div className="grid__description">
                    <p>{props.description}</p>
                </div>

                <footer className="grid__footer">
                    <ul className="grid__rating">
                        <li className="grid__rating__star grid__rating__star--active">★</li>
                        <li className="grid__rating__star grid__rating__star--active">★</li>
                        <li className="grid__rating__star grid__rating__star--active">★</li>
                        <li className="grid__rating__star grid__rating__star--active">★</li>
                        <li className="grid__rating__star">★</li>
                    </ul>

                    <a href="#share" className="grid__share">
                        <svg 
                            version="1.1" 
                            xmlns="http://www.w3.org/2000/svg" 
                            viewBox="0 0 24 24">
                            <path d="M13 10h3v3h-3v7h-3v-7h-3v-3h3v-1.255c0-1.189 0.374-2.691 1.118-3.512 0.744-0.823 1.673-1.233 2.786-1.233h2.096v3h-2.1c-0.498 0-0.9 0.402-0.9 0.899v2.101z"></path>
                        </svg>  
                    </a>
                    <div style={{clear: 'both'}}></div>                  
                </footer>
            </article>
        )
    }
}
    
export default GridItem;