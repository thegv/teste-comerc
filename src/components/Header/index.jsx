import React from 'react'
import './styles.css'
import * as userImage from '../../assets/images/user.png'

const Header = () => (
    <header className="header js-header">
        <div className="wrapper">
            <h1 className="header__logo hide-text icon-logo"><a href="#home">Pinball | The Grid Theme</a></h1>

            <button className="header__hamburguer js-open-menu">
                <span></span>
            </button>

            <button className="header__find">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="26" height="28" viewBox="0 0 26 28">
                    <path d="M18 13c0-3.859-3.141-7-7-7s-7 3.141-7 7 3.141 7 7 7 7-3.141 7-7zM26 26c0 1.094-0.906 2-2 2-0.531 0-1.047-0.219-1.406-0.594l-5.359-5.344c-1.828 1.266-4.016 1.937-6.234 1.937-6.078 0-11-4.922-11-11s4.922-11 11-11 11 4.922 11 11c0 2.219-0.672 4.406-1.937 6.234l5.359 5.359c0.359 0.359 0.578 0.875 0.578 1.406z"></path>
                </svg>
            </button>

            <ul className="header__navigation">
                <li><a href="#home">Home</a></li>
                <li><a href="#filmes">Filmes</a></li>
                <li><a href="#tvshow">TV Show</a></li>
            </ul>

            <form className="header__search" action="#">
                <div className="input">
                    <input type="text" />
                    <button className="header__search__submit" type="submit">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="26" height="28" viewBox="0 0 26 28">
                            <path d="M18 13c0-3.859-3.141-7-7-7s-7 3.141-7 7 3.141 7 7 7 7-3.141 7-7zM26 26c0 1.094-0.906 2-2 2-0.531 0-1.047-0.219-1.406-0.594l-5.359-5.344c-1.828 1.266-4.016 1.937-6.234 1.937-6.078 0-11-4.922-11-11s4.922-11 11-11 11 4.922 11 11c0 2.219-0.672 4.406-1.937 6.234l5.359 5.359c0.359 0.359 0.578 0.875 0.578 1.406z"></path>
                        </svg>
                    </button>
                </div>
            </form>

            <div className="header__user">
                <a href="#user">
                    <img src={userImage} alt="Guilherme Ventura" />
                    <span className="header__user__name">Guilherme Ventura</span>
                </a>
            </div>
        </div>
    </header>
)

export default Header;                